# jumpmark-msysgit
Quikly navigate your filesystem from Git Bash

A port/modification of Jeroen Janssens's marks tools to be used with Git Bash. See [the blog post](http://jeroenjanssens.com/2013/08/16/quickly-navigate-your-filesystem-from-the-command-line.html) for the original script. 

## Installation

There is two main ways to install Jumpmark:
	* Source the file from `.bashrc`
	* Append the contents to `.bashrc`

### Source the file
The file `jumpmark.sh` can  The contents of jump

### Append the file contents

## Usage

* `mark <name>` creates a jumpmark reference to the location. 
	```
	~/projects/jumpmark $ mark jumpmark
	```
* `unmark <name>` removes the jumpmark reference. 
* `jump <name>` 
* `marks` lists all jumpmarks. 
	```
	~/projects/jumpmark $ marks
	jumpmark
	projects
	tmp
	```
