export MARKPATH="$HOME/.local.marks"
function jump { 
    cd -P "$MARKPATH/$1" 2>/dev/null || echo "No such mark: $1"
}
function mark { 
    mkdir -p "$MARKPATH"
    WINPATH="$(echo $MARKPATH/$1 | tr '/' '\\' | cut -c3-)"
    cmd <<< "mklink /d $WINPATH %cd%" > /dev/null
}
function unmark { 
    rm -i "$MARKPATH/$1"
}
function marks {
    #ls -l "$MARKPATH" | sed 's/  / /g' | cut -d' ' -f9- | sed 's/ -/\t-/g' && echo
    ls "$MARKPATH"
}


#_completemarks() {
#  local curw=${COMP_WORDS[COMP_CWORD]}
#  local wordlist=$(find $MARKPATH -type l -printf "%f\n")
#  COMPREPLY=($(compgen -W '${wordlist[@]}' -- "$curw"))
#  return 0
#}
#complete -F _completemarks jump unmark
